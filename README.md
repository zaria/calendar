# Calendar

`calendar` plugin is a set of tools to manage events like appointments, meetings, events, etc.

It's using a `sqlite3` database under it.

## Installation

Nothing to do, that's working out of the box!

## Specification

Here are all available commands:

```
add_event(begin, end, title, location='', description='')
    add a new event in database
    
del_event(eventID)
    delete event from database
    
init()
    initialise database with following format:
      eventID:     INTEGER UNIQUE PRIMARY KEY
      begin:       TEXT (YYYY-MM-DD hh:mm)
      end:         TEXT (YYYY-MM-DD hh:mm)
      title:       TEXT
      location:    TEXT
      description: TEXT
    
mod_event(eventID, begin, end, title, location, description)
    modify an existing event from database
    
query_events_by_date(date)
    query all events with given date, expected date format: YYYY-MM-DD ('-DD' optionnal)
    
query_events_by_location(location)
    query all events with given location
    
query_events_by_title(title)
    query all events with given title
```
