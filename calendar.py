#!/usr/bin/env python3

"""
Calendar module
Just an interface between programs and SQL database
"""


import os
import re
import sqlite3


_CONN = sqlite3.connect(os.path.dirname(__file__) + "/calendar.db")
_CURSOR = _CONN.cursor()


def init():
    """
    initialise database with following format:
      eid:     INTEGER UNIQUE PRIMARY KEY
      begin:       TEXT (YYYY-MM-DD hh:mm)
      end:         TEXT (YYYY-MM-DD hh:mm)
      title:       TEXT
      location:    TEXT
      description: TEXT
    """
    _CURSOR.execute("DROP TABLE IF EXISTS events")
    _CURSOR.execute(
        """CREATE TABLE events
(
     "eid"       INTEGER UNIQUE PRIMARY KEY,
     "begin"         TEXT,
     "end"           TEXT,
     "title"         TEXT,
     "location"      TEXT,
     "description"   TEXT
)"""
    )
    _CONN.commit()


def _query_command():
    return "SELECT * FROM events WHERE {} LIKE '{}'"


def query_events_by_date(date):
    """
    query all events with given date, expected date format: YYYY-MM-DD ('-DD' optionnal)
    """
    if not re.fullmatch(r"(\d{4})-(\d{2})(-(\d{2}))?", date):
        raise ValueError(
            "given date isn't valid, expected: YYYY-MM-DD ('-DD' optional)"
        )
    else:
        return [
            event
            for event in _CURSOR.execute(
                _query_command().format("events.begin", date + "%")
            )
        ]


def query_events_by_location(location):
    """
    query all events with given location
    """
    return [
        event
        for event in _CURSOR.execute(
            _query_command().format("events.location", "%" + location + "%")
        )
    ]


def query_events_by_title(title):
    """
    query all events with given title
    """
    return [
        event
        for event in _CURSOR.execute(
            _query_command().format("events.title", "%" + title + "%")
        )
    ]


def add_event(begin, end, title, location="", description=""):
    """
    add a new event in database
    """
    regex_date = r"(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})"
    if not re.fullmatch(regex_date, begin) or not re.fullmatch(regex_date, end):
        raise ValueError("given date isn't valid, exptected: yyyy-mm-dd hh:mm")
    _CURSOR.execute(
        "INSERT INTO events (begin, end, title, location, description) VALUES ('{}', '{}', '{}', '{}', '{}')".format(
            begin, end, title, location, description
        )
    )
    _CONN.commit()


def del_event(eid):
    """
    delete event from database
    """
    _CURSOR.execute("DELETE FROM events WHERE eid='{}'".format(eid))
    _CONN.commit()


def mod_event(eid, begin, end, title, location, description):
    """
    modify an existing event from database
    """
    regex_date = r"(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})"
    if not re.fullmatch(regex_date, begin) or not re.fullmatch(regex_date, end):
        raise ValueError("given date isn't valid, exptected: yyyy-mm-dd hh:mm")
    _CURSOR.execute(
        "UPDATE events SET begin='{}', end='{}', title='{}', location='{}', description='{}' WHERE eid='{}'".format(
            begin, end, title, location, description, eid
        )
    )
    _CONN.commit()
